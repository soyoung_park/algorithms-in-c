#include <stdio.h>

int main(){
	int input;
	scanf("%d", &input);
	if (input == 1 || input == 3 || input ==5){
		printf("%s\n", "enjoy!");
	} else {
		printf("%s\n", "oops!");
	}
	return 0;
}

/*
프로그램 명: swimming
제한시간: 1 초

컴돌이는 월,수,금 수영을 신청했습니다.
월요일
화요일
수요일
목요일
금요일
토요일
일요일

만약의 경우를 위해 수영장 가는 날인지 아닌지를 체크해 주는 프로그램을 다음과 같이 작성하기로 했습니다.

1 에서 7 사이의 자연수가 입력으로 주어진다.
수영장 가는 날이면 enjoy , 아니면 oops 를 출력

입력

1

출력

enjoy

입력

4

출력

oops


*/

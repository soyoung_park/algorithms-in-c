#include <stdio.h>

int flip(int original){
	int backwards;
	while (original != 0){			// for 문이 아니라 while 문인 이유 설명.
		temp = original % 10;		// 나머지의 형태로 1의 자리수 저장하기. 
		original = original / 10;	// 이 부분 때문에 while 문의 조건문이 언젠가 멈춤 
		backwards = backwards*10 + temp; // 혹은 += 쓰기 
	}
	return backwards;
}

// candidate 이 prime 일 경우 
int isPrime(int candidate) {
	int i, returnval;
	returnval = 1; // 기본 값. 

	for (i = 2; i*i < candidate; i++) { // for 문을 거치면서 returnval 이 0으로 되어 return 되지 않았다면 
		if (candidate%i==0) {	 		// 테스트를 통과했음을 의미, 즉 소수이다. (나머지가 한번도 0이 아니었다)
			returnval = 0;
			return returnval;
		}
	}
	return returnval;
}

int main(){
	int number, i, flipped;
	scanf("%d", &number);

	flipped = flip (number);
	if (isPrime(number)==1 && isPrime(flipped)==1) {
		printf("%d is an emirp", number);
	} else if (isPrime(number)==1) {
		printf("%d is a prime", number);
	} else {
		printf("%d is not prime", number);
	} // if-else if-else 에서 주어진 숫자가 emirp, prime, 아무것도 아닐 경우가 차례로 걸러짐을 보여준다.  
	
	return 0;

	// 소수인지를 확인하는 함수 하나와 숫자를 뒤집어주는 함수 하나를 필요에 따라 부른다
}

/*

프로그램 명: emirps
제한시간: 1 초
prime numbers(소수) : 1 과 자신이외는 약수를 가지지 않는 수. 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31 , 37 ....
Emirps : 소수 중 뒤집힌 수도 소수인 수 37 은 Emirps 이다. ( 37 소수 , 73 도 소수)
수 를 입력으로 받아 not prime 인지 prime 인지 emirp 인지를 출력하는 프로그램을 작성하시오.
입력

입력으로 2 이상의 자연수가 n 이 입력으로 주어진다. n 은 20000000 이하이다.
출력

다음 세 가지 중 하나로 출력한다.
n is an emirp
n is a prime
n is not prime

*/
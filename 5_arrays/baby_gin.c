#include <stdio.h>

int card_input[6];

int is_run(int start, int finish){
	if (card_input[start+1]==card_input[start]+1 && card_input[finish]==card_input[start+1]+1) {
		return 1;
	} else {
		return 0;
	}
}

int is_triple_hit(int start, int finish){
	if (card_input[start]==card_input[start+1]==card_input[start+2]) {
		return 1;
	} else {
		return 0;
	}
}

int main() {

	int card_input[7];
	int i, j, k, temp;
	
	card_input[0]=0;

	for (i = 1; i < 7; i++) {
		scanf("%d", &card_input[i]);
	}

	for (j = 1; j < 6; j++){
		for (k = j + 1; k < 7; k++){
			if (card_input[j] > card_input[k]){
				temp = card_input[j];
				card_input[j] = card_input[k];
				card_input[k] = temp;
			}
		}
	}

	if((is_run(1,3)==1 && is_triple_hit(4,6)==1) || 
		(is_triple_hit(1,3)==1 && is_triple_hit(4,6)==1) ||
		(is_run(1,3)==1 && is_run(4,6)==1) ||
		(is_triple_hit(1,3)==1 && is_run(4,6)==1) ) {
		printf("win");
	} else {
		printf("lose");
	}

	return 0;
}


/*

프로그램 명: baby_gin(open)
제한시간: 1 초
Baby Gin 은 카드로 게임을 한다. 각 카드는 0 에서 9 까지의 수가 쓰여져 있다. 무늬는 없다. 게이머는 6 장의 카드를 받는다.

받는 사람이 Baby Gin 을 가진다면 이 게이머가 이기고 아니면 진다.

Baby Gin 을 가지기 위해서는 모든 6 장의 카드가 런(runs) 이거나 트리플리트(tripletes) 이여야 한다.

런이라는 말은 3 장의 카드가 연속적인 번호를 말한다. 345 혹은 789 .. , 단,901 은 런이아니다.
트리플리트는 같은 번호를 가지는 3 장의 카드를 말한다. 000 혹은 444,...
몇 가지 예를 보면 ,
667767 은 Baby Gin 이다. 두 개의 트리플리트(666,777)
054060 은 Baby Gin 이다. 하나의 런과 하나의 트리플리트
101123 은 Baby Gin 이 아니다.
6 개의 숫자를 입력으로 받아 Baby Gin 인지 아닌지를 판별하는 프로그램을 작성하는 것이 문제이다.
입력

0 에서 9 사이의 6 개의 숫자가 입력으로 주어진다.
출력

Baby Gin 이면 gin , 아니면 lose 를 출력한다.
입출력 예

입력

6 6 7 7 6 7

출력

gin

입력

0 5 4 0 6 0

출력

gin

입력

1 0 1 1 2 3

출력

lose

*/
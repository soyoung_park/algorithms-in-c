#include <stdio.h>

/*
input: 4 2
1100
1010
1001
0110
0101
0011
input: 5 3
11100
11010
11001
10101
10011
01110
01011
00111

*/

int n, k, i;
int arr[100];
i = 0;
//           n           k
void rec(int digits, int ones)
{
  i++;
  if (ones > k || digits > n) return;
  if (ones == k) printf(%d, arr);
  //next bit...
  arr[digits+1]=0; //takes 0
  rec(digits+1, ones);
  arr[digits+1]=1; //takes 1
  rec(digits+1, ones+1);
  arr[digits] = NULL; //clearing up the array
}

int main()
{
  scanf("%d %d", &n, &k)
    rec(0, 0);
  return 0;
}

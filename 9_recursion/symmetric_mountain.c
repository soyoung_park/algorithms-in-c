#include <stdio.h>


/*
input 3
output 12321

try to think of this as adding the '2' stack on top of '1' stack

*/

int n;

int rec_add(int current){
  if (n == current) printf(current);
  else {
    printf(current);
    rec_add(current+1);
    printf(current);
  }
}

int main(){
  scanf(%d, &n);
  rec_add(1);
  return 0;
}


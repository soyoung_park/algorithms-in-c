#include <stdio.h>

int bfs_queue[100000];
int distance[100000];

int n,k,i,current_node,counter,head,tail;

void enqueue(int new){
  bfs_queue[tail]=new;
  tail++;
}

int dequeue(){
  int saved = bfs_queue[head];//=0; //clear up the slot
  head++;
  return saved;
}


int main(){
  head = 0;
  tail = 0;
  counter=0;
  scanf("%d %d",&n, &k);

  enqueue(n);
  while ((tail-head)>0){ //while queue isn't empty
    current_node = dequeue();
    if (current_node ==k) break;

    if (distance[current_node-1]==0) {
      enqueue(current_node-1); //-1
      distance[current_node-1]=distance[current_node]+1;
    }
    if (distance[current_node+1]==0) {
      enqueue(current_node+1); //+1
      distance[current_node+1]=distance[current_node]+1;
    }
    if (distance[current_node*2]==0) {
      enqueue(current_node*2); //*2
      distance[current_node*2]=distance[current_node]+1;
    }
  }

  printf("%d\n", distance[current_node]);
  return 0;
}


/*

basically a separate array to keep track of the distances
if you were to utilize the original bfs_queue array to update the counter (distance), you would need to alternate between two queues for each 
level of the tree. the distance[] array also helps you eliminate unnecessary enqueues (repetitive ones) 
by checking if we've ever been to a 5 slot or 7 slot. if unexplored, 
well good, enqueue, but if not, what's the point of re-exploring the children of 5 when we already did that? 

*/

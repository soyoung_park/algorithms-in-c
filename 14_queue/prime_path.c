#include <stdio.h>
#include <math.h>

int start, end, current, front, tail, i, j, temp, counter;
int digits[4];
int queue[100];
int distance[100000];

int dequeue(){
  //printf("%s %d \n", "dequeue : ", queue[front]);
  int temp = queue[front];
  front++;
  return temp;
}

void enqueue(int neww){
  //printf("%s %d \n", "enqueue : ", neww);
  queue[tail] = neww;
  tail++;
}

int is_prime(int check_num){
  printf("%s %d \n", "is prime :", check_num);

  if (check_num == 1) return 0;
  for (i=2; i*i <= check_num; i++) {
    //if divisible by i, return 0
    if (check_num % i ==0) return 0;
  }
  return 1; //1 when prime
}

int main(){
  scanf("%d %d", &start, &end);
  front=0;
  tail=0;

  enqueue(start); 
  distance[start]=1;

  //filling in the queue iff temp is prime
  while (tail-front>0){
    printf("%s\n", "while loop");

    current = dequeue();
    counter=0;

    if (current==end) {
      printf("%d", distance[current]-1);
      break;
    }
    digits[0] = current % 10;               //one's digit
    digits[1] = current % 100 / 10;         //two's digit    
    digits[2] = current % 1000 / 100;       //three's digit  
    digits[3] = current / 1000;             //four's digit  

    for (i = 0; i < 4; i++){
      //printf("%s\n", "i = 0, 1, 2, 3");
      for (j=0; j < 10; j++){ //wydw 1: math operation. pow to calculate the 10^i, not just simple *
        counter++;
        printf("%s %d\n", "counter: ", counter);
        //printf("%s\n", "j = 0, 1, 2, 3, ... , 9");
      	temp = current-((digits[i] - j)*(int)pow(10, (double)i));    //clear up the particular digit (by subtracting)
        printf("%s %d\n", "temp is : ", temp);
      	if (is_prime(temp) && (temp > 1000) && (distance[temp]==0) ) { //is that prime? unvisited? not 0 _ _ _ ?
          //wydw 2: is_prime ran infinitely; why? 
          printf("%s\n", "***");
      	  enqueue(temp);                        //enqueue and go on.
      	  distance[temp]=distance[current]+1;   //update the counter
      	}
      }
    }
  }

  //dequeue
  
  
  return 0;
}

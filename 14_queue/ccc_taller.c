#include <stdio.h>

int reachable[1000000][2]; //100만. reachable[a]==b 일때 a>b
int bfs_visited[1000000];
int bfs_rev_visited[1000000];

//queue
int queue[1000000];
int head, tail;

int n,m;

int checker;


void enqueue(int input){
	queue[tail+1]=input;
	tail++;
	printf("tail is: %d", tail);
}

int dequeue(){
	head++;
	printf("head is: %d", head-1);
	return queue[head-1];
}
//queue

void bfs(int current, int destination){
	int temp, i;
	//mark current as visited. 
	bfs_visited[current]=1;
	//enqueue all unvisited children of the node
	printf("visiting node: %d\n", current);

	if (current == destination) {
		//print
		printf("%s", "yes");
		checker = 1;
		//stop the search
		return;
	}

	//find all reachable neighbors (children) of current node
	for (i=0; i<m; i++) { //O(m)
		printf("i=%d\n", i);
		if (reachable[i][0]==current) {
			printf("found child: %d\n", reachable[i][1]);
			//if the child has not been visited before,
			if (bfs_visited[reachable[i][1]]!=1) {
				enqueue (reachable[i][1]); //enqueue
				printf("enqueued: %d\n",reachable[i][1]);
			}
		}
	}

	//while queue ready (for current node) --> no the queue is universal
	while (tail!=head) {
		//visit each child
		temp = dequeue();
		printf("dequeued: %d\n",temp); //변수 실수한듯. look for typos and stuff
		bfs(temp, destination);
	}
}

void bfs_reverse_graph(int current, int destination){
	int i;
	//mark current as visited. 
	bfs_rev_visited[current]=1;

	if (current == destination) {
		printf("bfs_reverse_graph");
		//print
		printf("%s\n", "current==destination");
		checker = 1;
		//stop the search
		return;
	}

	//find all reachable neighbors (children) of current node
	for (i=0; i<m; i++) { //O(m)
		if (reachable[i][1]==current) {
			if (bfs_rev_visited[reachable[i][1]]!=1) {
				enqueue (reachable[i][1]); //enqueue
				printf("enqueued: %d\n",reachable[i][1]);
			}
		}
	}

	//while queue ready (for current node)
	while (tail!=head) {
		//visit each child
		bfs(dequeue(), destination);
	}
}

int main(){
	int first,second,p,q,i,j;
	scanf("%d %d", &n, &m);
	for (i=0; i<m; i++){
		scanf("%d %d", &first, &second);
		reachable[i][0] = first;
		reachable[i][1] = second; //2d array (matrix) but not 백만제곱, just 백만*2. 
	}
	scanf("%d %d", &p, &q); // is child(p) taller than child(q)?
	printf("=====\n");

	for (i=0;i<m;i++){
		for (j=0; j<2; j++){
			printf("%d ",reachable[i][j]);
		}
		printf("\n");
	}
	printf("=====\n"); // 

	//bfs [ start:p | fin:q ]
	head = 0;
	tail = 0;
	checker = 0;

	enqueue(p);
	bfs(p, q);

	if (head==tail){
		printf("empty queue\n");
	}

	bfs_reverse_graph(q, p);

	if (checker==0){ //after bfs and bfs on reverse graph, still remains unknown
		printf("unknown\n");
	}
	//bfs_neighbors[]

	return 0;
}



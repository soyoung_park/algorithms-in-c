#include <stdio.h>

int matrix[500][500];

int main(){
	int columns, rows, i, j, number; 
	scanf("%d %d", &columns, &rows);
	for (i=0; i<rows; i++){
		for (j=0; j<columns, j++){
			scanf("%d", &number);
			matrix[i][j]=number;
		}
	}

	for (j=0; j<rows; j++) {
		int straight_ones = 1;
		for (i=0; i<columns; i++) {
			if (matrix[i][j]!=1) {
				straight_ones = 0;
			}
		}
		if (straight_ones==1) {
			printf("yes \n");
			return 0;
		}
		
	}
	printf("no \n"); // 24 줄에서 return 0 을 하지 않았다면 여기에 오는 시스템 

	return 0;
}

/*

프로그램 명: plibrary
제한시간: 1 초
[문제 요약] 이차원 행렬이 주어진다. 열(column)중 모두 1 인 것이 하나라도 있으면 yes , 아니면 no 를 출력하는 프로그램을 작성.

입력

입력의 첫 줄은 열 과 행의 수이다. 열은 최대 100 까지 , 행은 최대 500 까지이다.
출력

입출력 예

입력

3 3
1 1 1
0 1 1
1 1 1

출력

yes

입력

7 2
1 0 1 0 1 0 1
0 1 0 1 0 1 0

출력

no
출처:South America 2005

*/
#include <stdio.h>

int main(){
	int n,i,j,k;

	scanf("%d", &n);

	for (i=0; i<n; i++) {
		if (i==0 || i==n/2 || i==n-1) {
			for (j=0; j<n; j++) {
				printf("%s", "*");
			}
		}
		else {
			for (j=0; j<n-1; j++){
				printf("%s", " ");
			}
			printf("%s", "*");
		}
		printf("\n");
	}
	return 0;
}

/*
프로그램 명: etnirp
제한시간: 1 초
홀수를 입력 받아 거울에 비친 E 를 출력하는 프로그램이다.
입력

홀수 n ( 5 <= n <= 19 ) 이 입력으로 주어진다.
출력

출력 예의 형식으로 출력한다. 불 필요한 공백을 출력해서는 안된다.
입출력 예

입력

5

출력

*****
    *
*****
    *
*****


*/
#include <stdio.h>

int main(){
	int n,i,j;
	scanf("%d", &n);
	for (i=n; i>0; i--){
		for (j=i; j>0; j--){
			printf("%s","*");
		}
		printf("\n");
	}
	return 0;
}

/*
프로그램 명: tri2
제한시간: 1 초
주어지는 줄 수 만큼 역 직각 삼각형을 출력하는게 문제이다.

입력

2 이상 20 이하의 자연수가 입력으로 주어진다.
출력

입출력 예

입력

5

출력

*****
****
***
**
*

*/
#include <stdio.h>

int main(){
	int n,i,j;
	scanf("%d", &n);

	for (i=0; i<n; i++){
		printf("%s", "*");
		for (j=1; j<n-1; j++) {
			
			if (i==0 || i==n-1) {
				printf("%s", "*");
			}
			else {
				printf("%s", " ");
			}
		}
		printf("%s\n", "*");
	}
	return 0;
}

/*
프로그램 명: nemo
제한시간: 1 초
수 n 을 입력으로 받아 n * n 네모를 출력하는 문제이다.

입력

n 이 입력으로 주어진다. n 은 3 이상 20 이하의 자연수이다.
출력

출력 예의 형식으로 출력한다. 불필요한 공백을 출력해서는 안된다.
입출력 예

입력

4

출력

****
*  *
*  *
****


*/
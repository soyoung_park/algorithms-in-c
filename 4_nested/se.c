#include <stdio.h>

int main(){
	int n,i,j,counter,msb;
	int number [1000000];
	scanf("%d", &n);

	counter=0;
	for (i=0;i<1000000; i++) {
		//say i = 127
		while (i/10!=0){
msb = get the most significant bit.
			counter++;
			number[counter]=msb;
			i=i/10;
		}
		for (j=0; j<)
	}


	return 0;
}

프로그램 명: se(open)
제한시간: 1 초
1부터 순서대로 입력된 긴 숫자가 있다.

123456789101112131415161718192021...
맨 왼쪽의 1을 첫 번째 자리로 시작하여, n 번째 자리에 무슨 숫자가 있는지 출력하시오.

위의 수열에서 예를 들면, 1을 첫 번째 자리로 하였을 때, n=3이면 3이고, n=25일 경우 7임을 알 수 있다.

입력

정수 n 이 입력된다. 1 <= n < 1,000,000
출력

n 번째 수를 출력한다.
입출력 예

입력

3

출력

3

입력

25

출력

7
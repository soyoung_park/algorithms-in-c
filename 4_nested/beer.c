프로그램 명: beer (special judge)
제한시간: 1 초
맥주를 좋아하는 클럽에서 정규모임을 가진다. 그들은 뜨뜨무리한 맥주를 아주 싫어한다. 그러나 클럽의 냉장고가 너무 작아서 충분한 양의 맥주를 보관할 수 없다.

그래서 그들은 큰 맥주 전용 냉장고를 주문하기로 했다. 새로운 냉장고는 a*b*c(가로*세로*높이) 인 직육면체 여야 한다.

냉기를 빼앗기지 않도록 냉장고의 겉면적은 가능한 작아야 한다.

예를 들어 , 부피가 12 인 냉장고여야 한다면 가능한 경우는

부피				겉 면적
3 × 2 × 2		32
4 × 3 × 1		38
6 × 2 × 1		40
12 × 1 × 1		50

이 경우 가장 좋은 선택은 3 × 2 × 2.

이를 도와 주는 프로그램을 작성하는 것이 일이다.

입력

입력으로 냉장고의 부피 정수 n 이 주어진다. ( 1 <= n <= 2 000 000 000 )
출력

겉 넓이를 최소로 하는 세 정수 a , b , c 를 출력한다. 답이 여러 개인 경우 그 중 하나 만을 출력하고 , 출력하는 순서는 중요하지 않다.
입출력 예

입력

12

출력

3 2 2

입력

13

출력

1 13 1

입력

1000000 

출력

100 100 100 
출처: Northeastern Europe 2007, Northern Subregion
#include <stdio.h>

int n,m, top;
int [1000000][2] llist;
int [100000] visited;
int [100000] index;
int [100000] stack;

void quicksort(int first, int last){ //int x[1000000],int first,int last){

	int pivot, current, wall;
	pivot = llist[last][0];
	current = first;
	wall = first;

	//purpose: pivot left <, right >
	if (first < last) { //first == last 안됨 
		while (current < last) {
			if (llist[current][0] < pivot) {
				current++;
			}
			else { //current greater than pivot
				//swap current with first element on the right of the wall. 
				int temp_0, temp_1;

				temp_0 = llist[current][0];
				llist[current][0]=llist[wall][0];
				llist[wall][0]=temp_0;

				temp_1 = llist[current][1];
				llist[current][1]=llist[wall][1];
				llist[wall][1] = temp_1;

				current++;
				wall++;
			}
		}
		quicksort(first,wall-1);
		quicksort(wall,last);
	}

/*
	int pivot,j,temp,i;

	if(first<last){
		pivot=first;
		i=first;
		j=last;

		while(i<j){
			while(x[i]<=x[pivot]&&i<last)
			 i++;
			while(x[j]>x[pivot])
			 j--;
			if(i<j){
			 temp=x[i];
			  x[i]=x[j];
			  x[j]=temp;
			}
		}

		temp=x[pivot];
		x[pivot]=x[j];
		x[j]=temp;
		quicksort(first,j-1);
		quicksort(j+1,last);

	}*/
}

void push(int input) {
	top++;
	stack[top]=input;
}

int pop(){
	int elmt = stack[top];
	top--;
	return elmt;
}

void dfs(int current){
	push(current);

	for (i=0; i<100000); i++) {
		int neighbor = llist[index[current]][i];
		if (visited[neighbor] != 1) {
			dfs(neighbor);
			printf("%s ", pop());
		}
	}

}

int main(){
	int i, from, to;
	scanf("%d %d", &n, &m);

	top=0;

	for (i=1; i<= 2*m; i=i+2) { // 1 3
		scanf("%d %d", &from, &to);
		llist[i][0]=from;	//1
		llist[i][1]=to;		//3
		llist[i+1][0]=to;	//3
		llist[i+1][1]=from;	//1
	} //done saving

	//sorting. quicksort takes O(mlogm).
	quicksort(1,2*m); //in-place quicksort.

	for (j=0;j<2*m; j++){
		for (i=0; i<n; i++) {
			if (llist[j][0]==i)
			index[i]=j;
		}
	}

	dfs(1);

	return 0;
}
/*
int [100000][100000] matrix;
int [100000] visited;


int main(){
	int from, to;
	scanf("%d %d", &n, &m);
	for (i=0; i< m; i++) {
		scanf("%d %d", &from, &to);
		matrix[from][to]=1;
		matrix[to][from]=1;
	}

	dfs(1);
	retrun 0;
}*/

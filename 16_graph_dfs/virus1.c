#include <stdio.h> //이런 문제는 dfs 말고 다른 탐색..? 으로도 풀수 있나요? 

int stack[100];
int connected[100][100];
int visited[100];
int stacked[100];

int vertices, edges;
int top; //stack[0] would be zero. null. nothing. nada. actual values would be from stack[1]. index one. 
int counter;

void push(int input){
	stacked[input]=1;
	top++;
	stack[top]=input;
	printf("push | %d\n", stack[top]);
}

int pop(){
	top--;
	printf("pop | %d\n", stack[top+1]);
	return stack[top+1];
}

void dfs(int current){
	//printf("exploring %d\n", current);

	int i;
	/*
	//mark current as visited
	visited[current]=1;
	counter++;

	//pop();

	//expand current : meaning expand it to a set of all its unvisited children.
	for (i=0; i< 100; i++){
		if (connected[current][i]==1){ //if connected
			if (visited[i]!=1 && stacked[i]!=1) {
				push(i);
			}
		}
	} //done finding all unvisited children.


	//now go to the top in stack
	if (top>0){
		dfs(pop());
	} else {
		printf("exiting\n");
		return;
	}*/

	/*push(current);
	while( top > 0 )
	{
		current = pop();
		visited[current]=1;
		counter++;		
		for (i=1; i<= vertices; i++){
			if (connected[current][i]==1){ //if connected
				if (visited[i]!=1 && stacked[i]!=1) {
					push(i);
				}
			}
		}
		printf("done exploring %d\n", current);
	}
	*/
	visited[current] = 1;
	counter++;
	for( i = 1 ; i <= vertices ; i++ )
	{
		if (connected[current][i]==1){ //if connected
			if (visited[i]!=1) {
				dfs(i);
			}
		}
	}

	//printf("done exploring %d\n", current);

}

int main(){
	int i, from, to;

	scanf("%d", &vertices);
	scanf("%d", &edges);

	for (i=0; i<edges; i++){
		scanf("%d %d", &from, &to);
		connected[from][to]=1;
		connected[to][from]=1;
	}

	top=0;
	counter=0;

	//push first element to stack
	//push(1);
	//run dfs
	//printf("current top of stack is :%d\n", top);
	dfs(1);

	printf("%d", counter);

	return 0;
}

/*

프로그램 명: virus1
제한시간: 1 초
신종 바이러스인 윔 바이러스는 네트워크를 통해 전파된다. 한 컴퓨터가 윔 바이러스에 걸리면 그 컴퓨터와 네트워크 상에서 연결되어 있는 모든 컴퓨터는 윔 바이러스에 걸리게 된다. 
예를 들어 7대의 컴퓨터가 < 그림 1 > 과 같이 네트워크 상에서 연결되어 있다고 하자. 1번 컴퓨터가 윔 바이러스에 걸리면 윔 바이러스는 2번과 5번 컴퓨터를 거쳐 3번과 6번 컴퓨터까지 전파되어 2, 3, 5, 6 네 대의 컴퓨터는 윔 바이러스에 걸리게 된다. 하지만 4번과 7번 컴퓨터는 1번 컴퓨터와 네트워크 상에서 연결되어 있지 않기 때문에 영향을 받지 않는다.
어느 날 1번 컴퓨터가 윔 바이러스에 걸렸다. 컴퓨터의 수와 네트워크 상에서 서로 연결되어 있는 정보가 주어질 때, 1번 컴퓨터를 통해 윔 바이러스에 걸리게 되는 컴퓨터의 수를 출력하는 프로그램을 작성하시오.

입력

첫째 줄에는 컴퓨터의 수가 주어진다. 컴퓨터의 수는 100 이하이고 각 컴퓨터에는 1번부터 차례대로 번호가 매겨진다.
둘째 줄에는 네트워크 상에서 직접 연결되어 있는 컴퓨터 쌍의 수가 주어진다.
이어서 그 수만큼 한 줄에 한 쌍씩 네트워크 상에서 직접 연결되어 있는 컴퓨터의 번호 쌍이 주어진다.

출력

1번 컴퓨터가 윔 바이러스에 걸렸을 때, 1번 컴퓨터를 통해 윔 바이러스에 걸리게 되는 컴퓨터의 수를 첫째 줄에 출력한다.

입출력 예

입력

7
6
1 2
2 3
1 5 
5 2
5 6
4 7

출력

4
출처:koi 초등기출

*/

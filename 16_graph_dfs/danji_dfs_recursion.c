#include <stdio.h>

int houses[25][25];
int counters[26]; // number of houses per group. there can be 25 groups max. 
int visited[25][25];

int i,j, n, cnt;
int group_index;

void print_map(int x, int y){
	for (i = 0; i < n ; i++) {
		for (j = 0; j < n; j++) {
			if (i==x && j==y) {
				printf("* ");
			} else {
				printf("%d ", houses[i][j]); 
			}
		}
		printf("\n");
	}
}



void dfs(int x, int y, int counter){
	if ( visited[x][y]!=1  &&  houses[x][y]!=0 &&  x>=0 && x<n  &&  y>=0 && y<n ) {
		visited[x][y]=1;
		printf("======map======\n"); 
		print_map(x,y); // 디버깅용 -- 현재 맵 찍어내기 
		printf("======map======\n\n");

		dfs (x, y-1, counter++);
		dfs (x, y+1, counter++);
		dfs (x-1, y, counter++);
		dfs (x+1, y, counter++);
	} 
}

int main(){
	scanf("%d", &n);
	group_index=0; 
	for (i = 0; i < n ; i ++) {
		for (j = 0; j < n; j++) {
			scanf("%d", &houses[i][j]); // !%s // %s 일때만 
		}
	}

	for (i = 0; i < n ; i ++) {
		for (j = 0; j < n; j++) {
			if (houses[i][j]!=0 && visited[i][j]!=1) {
				cnt=0;
				dfs(i,j,cnt);
				group_index++; 
				counters[group_index]=cnt;
				printf("%s %d \n", "reached the end of group:", group_index);
			}
		}
	}

	printf("%d \n", group_index);

	for (i=0; i<25; i++) {
		if (counters[i]!=0) {
			printf("%d ",counters[i]);
		}
	}
	return 0;
}


/*
프로그램 명: danji
제한시간: 1 초
아래와 같은 정사각형 모양의 지도가 있다. 1 은 집이 있는 곳을, 0 은 집이 없는 곳을 나타낸다.

0 1 1 0 1 0 0 
0 1 1 0 1 0 1 
1 1 1 0 1 0 1 
0 0 0 0 1 1 1 
0 1 0 0 0 0 0 
0 1 1 1 1 1 0 
0 1 1 1 0 0 0 

철수는 이 지도를 가지고 연결된 집들의 모임인 단지를 정의하고, 단지에 번호를 붙이려 한다. 
여기서 연결되었다는 것은 어떤 집이 좌우, 혹은 아래위로 다른 집이 있는 경우를 말한다. 
대각선상에 집이 있는 경우는 연결된 것이 아니다. 그림 2 는 그림1 을 단지별로 번호를 붙인 것이다. 
지도를 입력하여 단지수를 출력하고 , 각 단지에 속하는 집의 수를 오름차순으로 정렬하여 출력하는 프로그램을 작성하시오.

[그림 2]
0 1 1 0 2 0 0
0 1 1 0 2 0 2
1 1 1 0 2 0 2
0 0 0 0 2 2 2
0 3 0 0 0 0 0
0 3 3 3 3 3 0
0 3 3 3 0 0 0

입력 형식

첫 번째 줄에는 지도의 크기 N(5 <=N <= 25)이 입력되고
그 다음 N 줄에는 각각 N 개의 자료( 0 혹은 1)가 입력된다.
출력 형식

첫 번째 줄에는 총 단지수를 출력하시오. 
그리고 각 단지내의 집의 수를 오름차순 정렬하여 한 줄에 하나씩 출력하시오.
입출력 예

입력

7            
0 1 1 0 1 0 0
0 1 1 0 1 0 1
1 1 1 0 1 0 1
0 0 0 0 1 1 1
0 1 0 0 0 0 0
0 1 1 1 1 1 0
0 1 1 1 0 0 0

출력

3
7
8
9
출처: koi 초등부 기출 
*/
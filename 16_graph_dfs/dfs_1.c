
#include <stdio.h>

int matrix[10][10];
int visited[10];


void dfs(int current_node){
	int i;
	visited[current_node]=1;
	printf("%d\n", current_node); //visiting current node
	for (i=0; i<10; i++) {
		if (current_node == 8 && i !=6) {
			//printf("%d \n ", i);
			//printf("why isn't this printed \n");
		}

		if (visited[i]==0 && matrix[current_node][i]==1) {
			dfs(i); 
		}
	}
	//printf("exiting %d\n", current_node);
}


int main(){
	
	int num_nodes, start_node, i, j;

	scanf("%d %d", &num_nodes, &start_node);

	while ( scanf("%d %d", &i, &j) ) {
		if (i==0 && j==0) {
			//return 0;
			break; //from while loop, not from the entire function
		}
		matrix[i][j]=1;
		matrix[j][i]=1;

	}

	for (i=0; i<10; i++) {
		for (j=0; j<10; j++) {
			printf("%d ", matrix[i][j]);
		}
		printf("\n");
	}

	printf("%d \n", matrix[8][6]);

	//initialize the 'visited' array
	for (i=0; i<10; i++) {
		visited[i]=0;
	}

	dfs(start_node);


	return 0;
}
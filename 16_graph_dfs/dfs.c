#include <stdio.h>

int matrix[10][10];                 // 0 no edge   | 1 edge exists
int visit[10];                      // 0 unvisited | 1 visited      max number of nodes = 10
int num_nodes, start_node, cnt;

void dfs(int current){
  int i,j;
  visit[current]=1;
  //if (sizeof(matrix[current]) == 0 ) break; //break if it doesn't have any unvisited children
  printf("%d ", current);
  /*
  if (current == 5) {
    for (j=0; j<10; j++) {
      //printf("\n%s%d%s%d%s: ", "matrix[", current,"][",j,"]");
      //printf("%d", matrix[current][j]);
      printf("\n%s\n", "visit:");
      printf("%d", visit[j]);
    }
  }*/

  for (i=0; i< 10; i++) {
    if (matrix[current][i]==1) { //if edge exists
      if (visit[i]==0){
        //printf("%d %s\n",i, "에서 dfs 들어 "); // for 문의 i, j 는 휘발성으로 만 (지역변수)
        dfs(i);
/*
        printf("%d %s\n",i, "에서 dfs 한번 벗어남 ");
        for (j=0; j<10; j++) {
          printf("%s %d %d\n", "current:",  current,i);
          printf("%s %d\n","visit: ", visit[j]);
        }*/
      }
      //else {printf();}
    }
  }

}


int main(){ //adjacency matrix, i was concerned with not being able to go back, but recursion takes care of this
  int k,l;
  scanf("%d %d", &num_nodes, &start_node);
  while ( scanf("%d %d", &l, &k) ) { //!= EOF) { // 무엇이랑 비교를 해야 하는가 , be careful of the whitespace. scanf is like regex, unless the form isn't completely filled out it will act ugly
    if (l == 0 && k == 0) {
      break;
    } 
    matrix[k][l]=1;
    matrix[l][k]=1;
  }

/*
  for (i=0; i<10; i++){
    for (j=0; j<10; j++) {
      printf("%d ", matrix[i][j]);
    }
    printf("\n");
  }
*/
  for (k=0; k<10; k++){
    visit[k]=0;
  }

  dfs(start_node);
  return 0;
}
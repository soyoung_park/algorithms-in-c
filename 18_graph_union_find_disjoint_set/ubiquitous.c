#include <stdio.h>

int ancestor[50000];
int seen[50000];

//union and find -> always point at my ancestor

int union_find(int number){ //returns the root of number. recursively calls itself
	if (ancestor[number]==number) {
		return number;
	} 
	else {
		return union_find(ancestor[number]); // ? return union_find(ancestor[number]); ?
	}
}

int main(){
	int n,m,i,a,b, ancestors, different_rel;
	scanf("%d %d", &n, &m);
	ancestors=0;

	for (i=0; i<n; i++){
		ancestor[i]=i;
	}

	for (i=0; i<m; i++){
		scanf("%d %d", &a,&b);
		if (seen[ancestor[a]]!=1) { //if we haven't seen the ancestor of a...
			ancestors++;
			seen[ancestor[a]]=1;
		} 
		seen[b]=1;	
		ancestor[union_find(b)]=union_find(a); // update. 두 집단의 루트끼리 이어준다. 
		// 예를 들어 1-2, 2-3, 2-4 인 그래프와 5-6, 5-9, 6-7 인 그래프를 이어준다면 (new input is 2-6)
		// if you do ancestor[b]=union_find(a), 
		// then you're essentially connecting 1 with 6, 
		// unintentionally isolating the 5-9 component.
		// what SHOULD happen is you should connect 1 (=union_find(2)) with the ultimate parent (root) of 6. 
	}

	/* version 1. works but not recursive. 
	for (i=0; i<m; i++){
		scanf("%d %d", &a,&b);
		if (seen[ancestor[a]]!=1) { //if we haven't seen the ancestor of a...
			ancestor[a]=a;
			ancestors++;

			ancestor[b]=ancestor[a]; // update
			seen[ancestor[b]]=1;
			seen[b]=1;
			//seen[b]=1;
			
		} else { //if we have seen the ancestor of a...
			ancestor[b]=ancestor[a]; //(앞 = 부모, 뒤 = 자식) 이라는 전제
			seen[b]=1;
			//ancestors++;
			
		}
	}
	*/

	printf("%d\n", ancestors);

	different_rel=ancestors;

	for (i=1; i<=n; i++){
		if (seen[i]!=1) {
			printf("%s %d\n", "haven't seen ", i);
			different_rel++;
		}
	}

	//different_rel = 

	printf("answer : %d\n", different_rel);
	return 0;
}


/*
프로그램 명: ubiquitous
제한시간: 1 초

오늘날 아주 많은 다른 종교들이 있고 이들 모두를 추적하는 것은 어려운 일이다. 당신은 당신이 다니는 대학에 몇 가지의 서로 다른 종교가 있는 가를 알고자 한다.
대학에 n (0 < n <= 50000)명의 학생이 있다.
모든 학생들의 종교가 무엇인지를 물어보는 것은 힘든일이고 게다가 많은 학생들은 그들의 종교를 나타내는 것을 좋아하지 않는다.
이 문제를 해결하기 위한 한 가지 방법은 같은 종교를 가지는 사람들 끼리 짝을 짓도록 하는 것이다.

쌍의 수 m 은  이다.

이 데이터로 당신은 모든 학생들이 어떤 종교를 가지고 있는가는 알지 못하지만 당신은 다른 종교가 얼마나 있는가의 최대 한계는 알수 있다.
모든 학생들이 많아야 한 가지 종교를 가지고 있다고 하자.

[입력]
첫 줄에 정수 n , m 이 주어진다.
다음 m 라인은 두 정수 i , j 가 주어진다.
i , j 는 같은 종교를 가진 학생의 쌍이다.

[출력]
서로 다른 종교 수를 출력한다.

[입력]
10 9
1 2
1 3
1 4
1 5
1 6
1 7
1 8
1 9
1 10

[출력]
1

[입력]
10 4
2 3
4 5
4 8
5 8

[출력]
7

[권장 사항]
큰 테스트데이터가 입력으로 주어지므로 cin 보다는 scanf 로
출처:Alberta Collegiate Programming Contest 2003.10.18
*/
#include <stdio.h>

int top_height, top_start, current_pos;

int stack_height[100000];
int stack_start[100000];
int arr[100000];

long long area;

int peek(){
  return stack_height[top_height];
}

int pop(){
  int current_max_height;
  current_max_height=stack_height[top_height];                         // pop height stack
  area = current_max_height * (current_pos-stack_start[top_start]+1);  // calculate new area  
  top_height = top_height-1;                                           // update height top
  top_start = top_start-1;                                             // update start top
  return stack_start[top_start+1];
}

void push (int index){
  top_height++;
  top_start++;
  stack_height[top_height]=arr[index]; // top of stack takes input value.
  stack_start[top_start]=index; // or SOMETHING ELSE
}

int main(){
  int n,i;
  long long a;
  a=0;
  
  scanf("%d", &n);                                                    //read 7 2 1 4 5 1 3 3 
  for (i = 0; i < n; i++){
    scanf("%d",&arr[i]);                                              // this pointer conceptually equal to arr+i
  }

  push(0);
  
  printf("%s %d", "top of stack_height : ", peek());

  for (i = 1; i < n; i++){
    current_pos = i;
    if (arr[i]<peek()) {                                              // if next height cannot embrace the current one
      push(pop());
      if (area>a) {
	printf("%s %lld %lld\n", "updating answer from", a, area);
        printf("%s %d\n", "index is : ", i);
	a = area;
      }                                           // compare new area with max   
    } else {
      push (i);
    }
    // deal with what is left
    while (peek()!=0){
      pop();
    }
  }
  
  printf("%lld",a); // print answer
  return 0;
}



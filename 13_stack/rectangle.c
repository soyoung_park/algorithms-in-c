#include <stdio.h>

int arr[100000];

int main(){
  int n,i,j;
  long long a,min,prod;
  a=0;
  min=1000000000;
  scanf("%d", &n);
  for (i = 0; i < n; i++){
    scanf("%d",&arr[i]); // conceptually equal to arr+i
  }

  prod=0;

  i=0; //0 is NULL in C. '0' is char is not NULL equals 48 in ASCII code. 
  for (i = 0; i < n; i++){ // != 0 or !=NULL ? -> the former.
    printf("%s\n","entered loop");
    
    //for each i
    //starting from this position, all the way to the end of array
    for (j=i; j<n; j++){
      printf("%s\n","entered loop 2");
      if (arr[j]<min) {
        min=arr[j];
      }
      prod=min*(j-i+1);
      printf("%s %lld \n","current prod is: ",prod);
      if (prod > a) {
        printf("%s\n","updating answer");
        a = prod;
      }
    }

    /*
    if (arr[i-1]!=0 && arr[i-1]==arr[i]){
      maxcount++;
      prod = maxcount * arr[i];
      printf("%s\n","if");
    } 
    else {
      a=prod;
      prod=0;
      printf("%s\n","else");
    }
    i++;
    */
  }

  printf("%lld\n",a);
  return 0;
}

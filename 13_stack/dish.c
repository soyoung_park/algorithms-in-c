프로그램 명: dish(open)
제한시간: 1 초
접시가 a,b,c,d 순으로 한쪽이 막혀 있는 세척기에 들어간다고 할 때, b a c d 순으로 꺼내기 위해서는 push,push,pop,pop,push,pop,push,pop 하면 됩니다. d,a,b,c 순으로는 꺼낼수 없습니다.

세척기에서 꺼내는 순서가 주어질 때 동작을 보이시오.

입력

접시수는 30 개를 넘지 않고 차례대로 a,b,c,.... 로 소문자 알파벳으로 주어진다.

출력

가능한 경우 push , pop 의 순서를 아니면 impossible 을 출력한다.

입출력 예

입력

bacd

출력

push
push
pop
pop
push
pop
push
pop

입력 

dabc

출력

impossible
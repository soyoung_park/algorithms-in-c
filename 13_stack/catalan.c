#include <stdio.h>

int stack [100];
int arr [100];

int i,n,next,top;

int pop(){
  top--;
    printf("%s\n", "> popped. ");

  return stack[top];
}

void push(int new){
  stack[top]=new;
    printf("%s %d\n", "> pushed:", new);

  top++;
}

int main(){
  next=0;
  top=0;
  scanf("%d\n", &n); // 4
  for (i=0; i<n; i++){
    scanf("%d", &arr[i]);
  } //4 1 2 3


  for (i=0; i<n; i++) {
    printf("%s", "arr[next] : ");
    printf("%d\n", arr[next]);
    printf("%s", "top[stack] : ");
    printf("%d\n", stack[top-1]);

    push(i+1);

    while (arr[next]==stack[top-1] && top > 0) {
      next++;
      pop();
    }
  }
  if (next==n) {
    printf("%s", "YES");
  }
  else printf("%s", "NO");
  return 0;
}


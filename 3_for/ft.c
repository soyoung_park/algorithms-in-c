#include <stdio.h>

//def int MAX = 1000;

int main(){
	int i, input, num, sum, prod, prod_lsb;
	num = 0;
	sum = 0;
	prod = 1; 
	scanf("%d", &input);
	for (i = 0; i < 1000; i++) {
		if (input%i==0) //약수네용 
		{
			printf("");
			num = num + 1;
			sum = sum + i;
			prod = prod * i;
		}
	}

	prod_lsb = prod % 10;
	printf("\n");

	printf("%d\n", num);
	printf("%d\n", sum);
	printf("%d\n", prod_lsb);
	return 0;
}

/*

프로그램 명: ft
제한시간: 1 초
자연수가 입력으로 주어진다. 이 수의 약수를 출력하고, 
다음 줄에는 약수의 개수, 다음 줄에는 약수의 총합, 다음 줄에는 약수의 곱의 일의 자리수를 출력한다.

주어지는 수는 1000 이하의 자연수이다.

입력

6

출력

1 2 3 6
4
12
6

*/
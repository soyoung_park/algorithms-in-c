#include <stdio.h>


int main(){
	int i;
	float f, sum;
	sum = 0.0;
	for (i=0; i<12; i++) {
		scanf("%f", &f);
		sum = sum + f;
	}
	printf("%s%f\n","$", sum/12); //figure out how to print only the two digits below .
	return 0;
}

/*

프로그램 명: financial
제한시간: 1 초

입력은 12 줄로 이루어지고 , 각 줄당 그 달의 수입 금액이 주어진다. 모든 수는 양수이다. penny 단위로 주어지고 달러 기호는 포함되지 않는다.
출력

출력은 수 하나 , 지난 12 개월 동안의 평균 금액을 출력한다.
달러 기호을 먼저 출력 후 가장 가까운 penny 단위로 반올림하여 출력한다.

입력

100.00
489.12
12454.12
1234.10
823.05
109.20
5.27
1542.25
839.18
83.99
1295.01
1.75

출력

$1581.42

*/
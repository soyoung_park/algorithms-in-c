#include <stdio.h>

int main(){
	int first, second, i;
	scanf("%d %d", &first, &second);
	for (i=first; i<=second; i++){
		printf("%d ", i);
	}
	printf("\n");

	return 0;
}

/*
두 자연수가 주어진다. 두 수 사이의 수(두 수 포함)를 차례대로 출력하는 프로그램을 작성하시오.
두 수가 입력으로 주어진다. 두 수는 100 이하의 자연수이다.
두 수 사이에 공백을 하나 준다.

입력

3 6

출력

3 4 5 6

입력

6 3

출력

3 4 5 6
*/
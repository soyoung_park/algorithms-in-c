#include <stdio.h>

int main(){
	int i, number, sum;
	sum = 0;
	for (i=0; i<7; i++){
		scanf("%d", &number);
		sum += number;
	}
	printf("%d\n", sum);
	return 0;
}

/*
프로그램 명: sum
제한시간: 1 초
7 개의 자연수를 입력으로 받아 이 수들의 합을 구하는 프로그램을 작성하시오.
입력

7 개의 수가 입력으로 주어진다.
각 수는 100 이하의 자연수이다.

입력

6 2 9 8 3 4 7

출력

39
*/

#include <stdio.h>

/*

input:
THE QUICK BROWN FOX JUMPED OVER THE LAZY DOG.
THIS IS AN EXAMPLE TO TEST FOR YOUR
HISTOGRAM PROGRAM.
HELLO!

each input line will be less than 72, is ALL CAPS or special characters like . or !

output:
                            *
                            *
        *                   *
        *                   *     *   *
        *                   *     *   *
*       *     *             *     *   *
*       *     * *     * *   *     * * *
*       *   * * *     * *   * *   * * * *
*     * * * * * *     * * * * *   * * * *     * *
* * * * * * * * * * * * * * * * * * * * * * * * * *
A B C D E F G H I J K L M N O P Q R S T U V W X Y Z

source:USACO 2003 February Orange

*/

//each element in array corresponds to ascii alphabet's frequency. eg. arr[0] = frequency(A) :because -65
int arr[24]; // each element contains frequency, a decimal number. 

char one[73];
char two[73];
char three[73];
char four[73];

int main(){
  int count,i,j;
  count=0;

  fgets(one, 73, stdin);
  fgets(two, 73, stdin);
  fgets(three, 73, stdin);
  fgets(four, 73, stdin);
  
  for (i = 0; i < 73; i++){
    if (one[i]>=65 && one[i]<=90)// between 65 - 90
    arr[one[i]-65]++;
    if (count<(arr[one[i]-65])) count = arr[one[i]-65];
  }

  for (i = 0; i < 73; i++){
    if (two[i]>=65 && two[i]<=90)// between 65 - 90   
    arr[two[i]-65]++;
    if (count<(arr[two[i]-65]))count =arr[two[i]-65];
  }

  for (i = 0; i < 73; i++){
    if (three[i]>=65 && three[i]<=90)// between 65 - 90   
    arr[three[i]-65]++;
    if (count<(arr[three[i]-65]))count =arr[three[i]-65];
  }

  for (i = 0; i < 73; i++){
    if (four[i]>=65 && four[i]<=90)// between 65 - 90   
    arr[four[i]-65]++;
    if (count<(arr[four[i]-65]))count =arr[four[i]-65];
  }

  for(i=0; i<24; i++){
    printf("%d",arr[i]);
  }

  /*
  for (i=0; i<24; i++){
    for (j=0; j<count; j++){
      if (arr[i]<=count){
	printf("%s","*");
      }
    }
  }
  */

  return 0;
}

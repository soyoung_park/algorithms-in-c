//implement this in both kruskal and prim

#include <stdio.h>

// prim -> expand the 'cut' by choosing the safest (lightest) edge that does not create a cycle
// kruskal -> connect the isolated vertices (nodes) by choosing the vertex with the smallest weight, universally. without creating a cycle of course. 

int farms[100][100];

int min_length_prim;
int spanning_tree[100];


int min_length_kruskal;
int spanning_tree_kruskal[100];

int kruskal_save[10000][3];

void union_find_prim(int added) {
	printf("entered uf, node added was : %d \n", added);
	int i, j, shortest_distance,next, temp;
	shortest_distance=100000;
	next=0;

	spanning_tree[added]=1;
	for (i = 1; i <= 100; i++){
		for (j = 1; j <= 100; j++){
			if (spanning_tree[i]==1) { //if the node is part of the tree
				temp = farms[i][j];
				//printf("%d\n", temp);
				if (temp != 0 && temp < shortest_distance && spanning_tree[j]!=1) {
					printf("얄루 %d \n", temp);
					shortest_distance = temp; 
					next=j;
				}
			}
			
		}
	}
	
	if (next != 0) {
		min_length_prim += shortest_distance;
		printf("min length added by %d \n ", shortest_distance);
		union_find_prim(next);
	}

	printf("exiting uf \n");
	
}

void union_find_prim_old(int node_added, int shortest_outbound, int closest_node){
	printf("* entered union_find_prim; node added was %d, shortest was %d, closest node next is %d .\n", node_added, shortest_outbound, closest_node);
	int i, dist, next_dist;

	if (closest_node==0) {
		return;
	}

	int closest = 0;
	for (i = 1; i <= 100; i++){
		dist= farms[node_added][i];

		if (dist != 0 && dist < shortest_outbound && spanning_tree[i] != 1){
			printf("dist is %d \n", dist);
			shortest_outbound = dist;
			closest = i;
		}
	}
	//update the shortest_outbound and closest_node;
	if (closest !=1){
		node_added=closest;
		spanning_tree[node_added]=1;
		for (i = 1; i <= 100; i++){
			next_dist= farms[node_added][i];
			if (next_dist != 0 && next_dist < shortest_outbound && spanning_tree[i] != 1){
				shortest_outbound = next_dist;
				closest_node = i;
			}
		}
		
	}
	else {
		node_added=closest_node;
		//shortest_outbound = ?;
		//closest_node = ?;

		for (i = 1; i <= 100; i++){
			next_dist= farms[closest_node][i];
			if (next_dist != 0 && next_dist < shortest_outbound && spanning_tree[i] != 1){
				shortest_outbound = next_dist;
				closest_node = i;
			}
		}
	} 

	printf("* * recursive enter union_find_prim\n");
		union_find_prim_old(node_added, shortest_outbound, closest_node);

	node_added=closest;

	printf("closest is : %d", closest);
	
	printf("ideally should exit recursion \n");

}


void union_find_kruskal(){
	int next_shortest;
	next_shortest = ; // first in the sorted list of edges
	to=kruskal_save[next_shortest][1];
	fro=kruskal_save[next_shortest][2];
//if (union_find (to, fro)) {add to the tree}



}

// at each iteration add the cheapest line that does not create a cycle.
// if adding a new line should create a cycle, quit. 
void union_find_kruskal_recursion() {
	int shortest_universal, new_node_to, new_node_fro, i, j, temp;
	shortest_universal = 100000;
	new_node_to=0;
	for (i=1; i<=100; i++){
		for (j=1; j<=100; j++){

			temp=farms[i][j];
			if (temp != 0 ) {
				printf("temp is %d \n", temp);
			}
			if (temp != 0 && temp < shortest_universal && (spanning_tree_kruskal[j]==1 && spanning_tree_kruskal[i]==1) ) { //good thinking, but what if to and fro each belong to a different tree?
				shortest_universal = temp; 
				new_node_to = j;
				new_node_fro = i;
			}
		}
	}
	if (new_node_to!=0){
		min_length_kruskal += shortest_universal;
		spanning_tree_kruskal[new_node_to]=1;
		spanning_tree_kruskal[new_node_fro]=1;
		printf("added %d, distance increase by %d \n", new_node_to, shortest_universal);
		union_find_kruskal();
	}
}

int main(){
	int n,i,j, distance, shortest, nextt, counter;
	shortest=100000;

	scanf("%d", &n);
	counter=0;
	for (i=1; i<=n; i++){
		for (j=1; j<=n; j++) {
			counter++;
			scanf("%d", &distance);
			farms[i][j]=distance;
			kruskal_save[counter][0]=distance;
			kruskal_save[counter][1]=i;
			kruskal_save[counter][2]=j;
		}
	}

	union_find_prim(1);
	printf("=============kruskal============= \n");
	for (){
		union_find_kruskal();
	}

	printf("최소 거리 (prim) : %d\n", min_length_prim);
	printf("최소 거리 (kruskal) : %d\n", min_length_kruskal);

	return 0;
}


/*

프로그램 명: agrinet
제한시간: 1 초
철수는 그의 마을에서 이장으로 선출되었다. 그의 선거공약 중의 하나는 마을 전체를 인터넷으로 연결하겠다는 것이다.

그는 초고속 망을 들여와서 다른 집과 연결 하려고 한다. 
비용을 최소화하기 위해 최소한의 선을 이용하여 모든 집을 연결하려 한다.모든 농가는 다른 농가와 연결되어야 한다.
두 농가사이의 거리는 100,000 을 넘지 않는다.

[입력]
첫 줄은 농가의 수 N ( 3 <= N <= 100 )이 주어지고 , 다음 줄 부터는 N * N 의 배열이 주어진다. 각 배열은 각 농가까지의 거리를 나타낸다.

[출력]
가장 최소 거리를 출력한다.
입출력 예

[입력]
4
0 4 9 21
4 0 8 17
9 8 0 16
21 17 16 0

[출력]
28

출처: usaco

*/
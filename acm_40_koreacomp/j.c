#include <stdio.h>
#include <math.h>


//피타고라스 기대값 -> 야구팀 승률 문제  
int gain_loss[1000][2];

int main(){
	int i, j, t, n, m, a, b, p, q;
	scanf("%d", &t);
	for (i=0; i<t; i++) {
		int p_expectancy, min_expectancy, max_expectancy;

		scanf("%d %d ", &n, &m);
		for (j=0; j<m; j++) {
			scanf("%d %d %d %d", &a, &b, &p, &q);
			gain_loss[a][0] += p;
			gain_loss[a][1] += q;
			gain_loss[b][0] += q;
			gain_loss[b][1] += p;
		}
			//printf("%f \n", pow(gain_loss[1][0], 2) ); 
			min_expectancy = (1000 * ( pow(gain_loss[1][0], 2) ) ) /  ( pow(gain_loss[1][0], 2)  + pow(gain_loss[1][1], 2) ) ;
			//printf("%d \n", min_expectancy);
			max_expectancy = (1000 * ( pow(gain_loss[1][0], 2) ) ) /  ( pow(gain_loss[1][0], 2)  + pow(gain_loss[1][1], 2) ) ;
			//printf("%d \n", max_expectancy);
		for (j=1; j<=n; j++) {
			p_expectancy = (1000 * ( pow(gain_loss[j][0], 2) ) ) /  ( pow(gain_loss[j][0], 2)  + pow(gain_loss[j][1], 2) ) ;
			if (p_expectancy > max_expectancy) {
				max_expectancy = p_expectancy;
			}
			if (p_expectancy < min_expectancy) {
				min_expectancy = p_expectancy;
			}

		}
		printf("%d \n", max_expectancy);
		printf("%d \n", min_expectancy);

		// clear up
		for (j=1; j<=1000; j++) {
			gain_loss[j][0]=0;
			gain_loss[j][1]=0;
		}

	}
	return 0;
}